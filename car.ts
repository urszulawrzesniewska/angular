interface Vehicle {
    name: String;
    speed: number;
    wheels: number;
    
    forward():void;
    stop():void;
    }
    
    class Car implements Vehicle {
    
        constructor(public name: string, public speed: number, public colour: string, public wheels: number) {
        }

        forward():void {
            alert(`${this.colour} ${this.name} moved forward`)
        }
        stop():void{
            alert(`${this.colour} ${this.name} stopped`)
        }
    }
    
    let car2 = new Car("Opel", 50, "red", 4);
    car2.forward();
    car2.stop();
    
    class Porsche extends Car{
        constructor(name: string,speed: number, colour: string, wheels: number) {
            super(name, speed, colour, wheels);
        }
    }
    
    let porsche2 = new Porsche("Cayenne",100, "blue", 4);
    porsche2.forward();
    porsche2.stop();