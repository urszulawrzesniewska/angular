import { Component } from '@angular/core';
import { Todo } from '../app/todo'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  todos: Array<Todo>;
  currentTodo: Todo;

  constructor() {
    this.todos = [];
    this.currentTodo = new Todo("","");

  }
  add(todo:Todo) {
    this.todos.push(todo);
  }

  select(todo:Todo) {
    this.currentTodo = todo;
  }
}
