import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo'

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @Input()
  todos: Array<Todo>;

  @Output()
  todoSelector: EventEmitter<Todo>;

  constructor() {

    this.todoSelector = new EventEmitter();

   }

  ngOnInit() {
  }

  selectTodo(todo:Todo)  {
    this.todoSelector.emit(new Todo(todo.title,todo.note));
  
  }

}
