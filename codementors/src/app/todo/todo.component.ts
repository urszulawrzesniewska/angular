import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  ngOnInit(){
  }

  @Input()
  todo: Todo;

  @Output()
  todoAdded: EventEmitter<Todo>;

  constructor() { 
    this.todoAdded = new EventEmitter();
  }


  add(todo:Todo) {
    this.todoAdded.emit(new Todo(todo.title,todo.note));
    this.todo = new Todo('','')
    this.todo.title='';
    this.todo.note='';

  }
}
