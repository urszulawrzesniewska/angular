function greeter(name: string) : string{
    return "Hello " + name;

}

var username = "Ula";

document.body.innerHTML = greeter(username);

class cat {
    firstName: string;
    weight: number;

}

var klusek = new cat();

let book:string = "Modyfikowany Wegiel";
let counter: number = 5;
const summary: string = `na magazynie mamy ${counter +1 }
ksiazek o tytule ${book}`

document.body.innerHTML = summary;

let lista : Array<Number> = [8,88,888]
for(let i in lista) {
    console.log(i); //klucze
}
for (let i of lista) {
    console.log(i); //wartosci
}

interface TitleValue {
    title: string
}
function showTitle(titleObject: TitleValue){
    alert(titleObject.title);
}
let book2 = {
    title: "Modyfikowany Wegiel",
    author: "Richard Morgan"
}
showTitle(book2);

class ComicBook implements TitleValue {
    title: string;

}
var comic = new ComicBook();
comic.title = "Spiderman";

function showTitle2(titleObject: ComicBook) {
    alert(titleObject.title);
}

showTitle(comic);

interface Animal {
    name: String;
    move():void;
}

class Dog implements Animal {

    constructor(public name: string, private steps: number) {
    }
    
    move():void {
        alert(`${this.name} moved ${this.steps}`)
    }
}

let dog = new Dog("Nero", 5);
dog.move();

class Shepherd extends Dog{
    constructor(name: string,steps: number) {
        super(name, steps);
    }
    private _age: number

    public get age(): number {
        return this._age
    }
    public set age(v: number) {
        alert("new age")
        this._age = v;
    }
}

let shepherd = new Shepherd("Bob",8);
shepherd.move();
shepherd.age=7;