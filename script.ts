//fighter
interface Fighter{

    name: string;
    HP: number;
    accuracy: number;
    defence: number;
    attack: number;
    strength: number;
    accDc: number;
    initiative: number;
}
abstract class Person implements Fighter {

    constructor(public name: string, HP: number, 
        public accuracy: number, public defence: number, public attack: number,
        public strength: number, public accDc: number, public initiative: number) {
            this._hpCurrent=this._maxHp=HP;
        }

        private _hpCurrent: number;
        public _maxHp: number;
        public isAlive= true;

        public get HP(): number {
            return this._hpCurrent;
        }

        public set HP(_hp: number) {
            this._hpCurrent = _hp;
            this.isAlive = this._hpCurrent>0;
        }
    abstract hit(): void;
    abstract atk(target: Person): void;
    abstract specialAtk(target: Array<Person>);

}

class Warrior extends Person {
    specialAtk(target: Person[]) {
        throw new Error("Method not implemented.");
    }
    constructor(name: string, HP: number, accuracy: number, defence: number, attack: number,
         strength: number, accDc: number, initiative: number) {
            super(name,HP,accuracy,defence,attack,strength,accDc,initiative);
        }

    hit():boolean {
        return this.accuracy > rollDice(100);
    };

    atk(target: Person):void {
        if(this.hit()){
            let attackValue = (this.strength + rollDice(this.attack) - target.defence);
            target.HP -= attackValue;
            console.log(`${this.name} attacks ${target.name} for ${attackValue} hp`)
        }
        else console.log("miss")
    };

}

class Cleric extends Warrior {
    specialAtk(target: Person[]) {
       
    }
    heal(target: Person[]): any {
        for(let i=0;i<target.length;i++) {
            target[i].HP =+30;
        }
    }

    constructor(name: string, HP: number, accuracy: number, defence: number, attack: number,
        strength: number, accDc: number, initiative: number) {
           super(name,HP,accuracy,defence,attack,strength,accDc,initiative);
}
}
class Dragon extends Person {
    specialAtk(target: Person[]) {
        for(let i=0; i< target.length;i++) {
            this.atk(target[i]);

        }
    }
    constructor(name: string, HP: number, accuracy: number, defence: number, attack: number,
        strength: number, accDc: number, initiative: number) {
           super(name,HP,accuracy,defence,attack,strength,accDc,initiative);
       }
       hit():boolean {
        return this.accuracy > rollDice(100);
    };

    atk(target: Person):void {
        if(this.hit()){
            let attackValue = (this.strength + rollDice(this.attack - target.defence));
            target.HP -= attackValue;
            console.log(`${this.name} attacks ${target.name} for ${attackValue} hp`)
        }
        else console.log("miss")
    };

}
    function rollDice(max: number):number {
        return Math.floor((Math.random() * max) + 1);
    }
    function initCalc():number {
        return rollDice(10);

    }
    function combat(): any {
        let team: Array<Warrior>;
        team = [
            new Warrior("Aragorn",100,80,10,10,50,10,10),
            new Warrior("Imoen",100,90,10,10,50,10,10),
            new Warrior("Keldorn",100,70,10,10,50,10,10),
            new Warrior("Minsc",100,60,10,10,50,10,10),
            new Cleric("Viconia",100,80,10,10,100,10,10)
        ];

        let dragon = new Dragon("Firkraag",1000,90,10,100,100,10,10);

        while(true){
            //everybody dies
            let counter = 0;
            for(let i=0;i<team.length;i++){
                if(team[i].isAlive && dragon.isAlive) {
                    if(team[i] instanceof Cleric) {
                        (team[i] as Cleric).heal(team);
                        console.log(team[i].name +" healed party")
                    }
                    team[i].atk(dragon);
                    console.log("dragon hp is:" +dragon.HP);

                }
                else counter++;
            }
            if (counter == team.length) break;
            if(dragon.isAlive) {
                dragon.specialAtk(team);

            } else {
                break;
            }

    }
}

combat();