class Person {
    constructor(name, HP, accuracy, defence, attack, strength, accDc, initiative) {
        this.name = name;
        this.accuracy = accuracy;
        this.defence = defence;
        this.attack = attack;
        this.strength = strength;
        this.accDc = accDc;
        this.initiative = initiative;
        this.isAlive = true;
        this._hpCurrent = this._maxHp = HP;
    }
    get HP() {
        return this._hpCurrent;
    }
    set HP(_hp) {
        this._hpCurrent = _hp;
        this.isAlive = this._hpCurrent > 0;
    }
}
class Warrior extends Person {
    specialAtk(target) {
        throw new Error("Method not implemented.");
    }
    constructor(name, HP, accuracy, defence, attack, strength, accDc, initiative) {
        super(name, HP, accuracy, defence, attack, strength, accDc, initiative);
    }
    hit() {
        return this.accuracy > rollDice(100);
    }
    ;
    atk(target) {
        if (this.hit()) {
            let attackValue = (this.strength + rollDice(this.attack) - target.defence);
            target.HP -= attackValue;
            console.log(`${this.name} attacks ${target.name} for ${attackValue} hp`);
        }
        else
            console.log("miss");
    }
    ;
}
class Cleric extends Warrior {
    specialAtk(target) {
    }
    heal(target) {
        for (let i = 0; i < target.length; i++) {
            target[i].HP = +30;
        }
    }
    constructor(name, HP, accuracy, defence, attack, strength, accDc, initiative) {
        super(name, HP, accuracy, defence, attack, strength, accDc, initiative);
    }
}
class Dragon extends Person {
    specialAtk(target) {
        for (let i = 0; i < target.length; i++) {
            this.atk(target[i]);
        }
    }
    constructor(name, HP, accuracy, defence, attack, strength, accDc, initiative) {
        super(name, HP, accuracy, defence, attack, strength, accDc, initiative);
    }
    hit() {
        return this.accuracy > rollDice(100);
    }
    ;
    atk(target) {
        if (this.hit()) {
            let attackValue = (this.strength + rollDice(this.attack - target.defence));
            target.HP -= attackValue;
            console.log(`${this.name} attacks ${target.name} for ${attackValue} hp`);
        }
        else
            console.log("miss");
    }
    ;
}
function rollDice(max) {
    return Math.floor((Math.random() * max) + 1);
}
function initCalc() {
    return rollDice(10);
}
function combat() {
    let team;
    team = [
        new Warrior("Aragorn", 100, 80, 10, 10, 50, 10, 10),
        new Warrior("Imoen", 100, 90, 10, 10, 50, 10, 10),
        new Warrior("Keldorn", 100, 70, 10, 10, 50, 10, 10),
        new Warrior("Minsc", 100, 60, 10, 10, 50, 10, 10),
        new Cleric("Viconia", 100, 80, 10, 10, 100, 10, 10)
    ];
    let dragon = new Dragon("Firkraag", 1000, 90, 10, 100, 100, 10, 10);
    while (true) {
        //everybody dies
        let counter = 0;
        for (let i = 0; i < team.length; i++) {
            if (team[i].isAlive && dragon.isAlive) {
                if (team[i] instanceof Cleric) {
                    team[i].heal(team);
                    console.log(team[i].name + " healed party");
                }
                team[i].atk(dragon);
                console.log("dragon hp is:" + dragon.HP);
            }
            else
                counter++;
        }
        if (counter == team.length)
            break;
        if (dragon.isAlive) {
            dragon.specialAtk(team);
        }
        else {
            break;
        }
    }
}
combat();
